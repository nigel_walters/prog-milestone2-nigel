﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_milestone2_nigel
{
    /*
    Grade Average
      Enter student details
        Ask the user for their student ID 
        Ask the user if they are level 5 or level 6
        If the user is level 5 the user needs to enter in 4 papers
        If the user is level 6 the user needs to enter in 3 papers
        Next the user needs to type in paper code and the mark for that paper

    */
    class GradeAverage
    {
        // Main entry point to run tasks
        public static void Run(string taskName)
        {
            // Task menu
            Dictionary<int, string> gradeMenu = new Dictionary<int, string>();
            // Task heading
            gradeMenu.Add(0, taskName);
            // Task menu items
            gradeMenu.Add(1, "Student Details");
            gradeMenu.Add(2, "Grade Average");
            gradeMenu.Add(3, "A+ Grade");

            // Declare variables
            int select;
            string value;

            // Instantiate student object
            Student student = new Student();

            // Display heading to enter student details
            Program.Heading("Enter Student Details");

            // Assign student to enter details method
            student = EnterDetails(student);

            // Display menu for tasks/methods to do
            do
            {
                // Display task menu method
                select = Program.DisplayMenu(gradeMenu);

                // Check to see if the value exists in the menu dictionary
                gradeMenu.TryGetValue(select, out value);

                // Run method if selection is valid, infers that value is also valid
                switch (select)
                {
                    case 1:
                        StudentDetails(student, value);
                        break;
                    case 2:
                        AverageGrade(student, value);
                        break;
                    case 3:
                        PlusGrade(student, value);
                        break;
                }

            } while (select != gradeMenu.Count);
            return;
        }

        // Enter student details method
        public static Student EnterDetails(Student s)
        {
            // Variable to check for parsed values
            bool check;
            // Create list of papers
            s.papers = new List<Paper>();

            // Display message to enter student id
            Console.Write("Please enter your student id: ");
            // Assign student id
            s.studentID = Console.ReadLine();

            // Prompt for course level while it is a valid value
            do
            {
                Console.Write("Please enter your course level 5 or 6: ");
                check = int.TryParse(Console.ReadLine(), out s.courseLevel);
            } while (s.courseLevel != 5 && s.courseLevel != 6);

            // Formula for number of papers
            s.numberOfPapers = s.courseLevel - 2;
            
            // Prompt for paper codes
            Console.WriteLine($"Please enter {s.numberOfPapers} paper codes");

            // Iterate through loop by number of papers
            for (int i = 0; i < s.numberOfPapers; i++)
            {
                Console.Write($"Paper {i + 1}: ");
                Paper p = new Paper();
                p.name = Console.ReadLine();
                s.papers.Add(p);
            }

            // Prompt for percentage marks of papers
            Console.WriteLine("Please enter paper percentage marks");

            // Go through each paper to display and enter percentage marks
            foreach (var paper in s.papers)
            {
                // Loop to make sure that the input is valid
                do
                {
                    Console.Write($"{paper.name}: ");
                    check = int.TryParse(Console.ReadLine(), out paper.mark);
                    if (paper.mark >= 0 && paper.mark <= 100)
                    {
                        paper.grade = GetGrade(paper.mark);
                    }
                } while (!check || paper.mark < 0 || paper.mark > 100);
            }
            return s;
        }

        // Display summary of student details method
        internal static void StudentDetails(Student s, string taskHeading)
        {
            Program.Heading(taskHeading);
            Console.WriteLine($"Student ID: {s.studentID}");
            Console.WriteLine($"Course Level: {s.courseLevel}");
            Console.WriteLine("Papers:");
            foreach (var p in s.papers)
            {
                Console.Write($"\n");
                DisplayGrade(p.name, p.mark, p.grade);
            }
            Program.PauseScreen();
            return;
        }

        // Get average grade based on paper marks
        private static void AverageGrade(Student s, string taskHeading)
        {
            // Initialise variables
            int total = 0;
            int average;

            // Loop through papers to total marks
            foreach (var p in s.papers)
            {
                total += p.mark;
            }

            // Average marks from total and number of papers
            average = total / s.numberOfPapers;

            // Display average grade
            DisplayGrade(taskHeading, average, GetGrade(average), "*", clear: true);
            Program.PauseScreen();
            return;
        }

        // Check to see if the studen has any A+ grades
        private static void PlusGrade(Student s, string taskHeading)
        {
            // Initialise variables
            bool check = false;
            int total = 0;

            // Display task heading
            Program.Heading(taskHeading);

            // Go through the papers and check if any are A+
            foreach (var p in s.papers)
            {
                // Display paper if has an A+ grade
                if (p.grade == "A+")
                {
                    check = true;
                    DisplayGrade(p.name, p.mark, p.grade);
                    total++;
                }
            }

            // If no A+ grades
            if (!check)
            {
                Console.WriteLine("You have no A+ grades");
            }
            // Otherwise... Display the amount of papers that do
            else
            {
                Console.WriteLine($"\nCongratulations! You have {total} paper(s) with an A+");
            }

            Program.PauseScreen();
            return;
        }

        // Shared method to display grade
        private static void DisplayGrade(string heading, int percent, string grade, string special = "-", bool clear = false)
        {

            Program.Heading(heading, special, clear);
            Console.WriteLine($"Percent: {percent}");
            Console.WriteLine($"Grade: {grade}");
        }

        // Shared method to determine grade from mark percentage
        private static string GetGrade(int mark)
        {
            // Initialise variables
            string grade = "";
            int[] marks = { 100, 85, 80, 75, 70, 65, 60, 55, 50, 40 };
            string[] grades = { "A+", "A", "A-", "B+", "B", "B-", "C+", "C", "D", "E" };

            // Loop through to check if mark is grade
            for (int i = 0; i < marks.Length; i++)
            {
                if (mark < marks[i])
                {
                    grade = grades[i];
                }
            }
            return grade;
        }

        // Student class to hold student properties
        public class Student
        {
            public string studentID;
            public int courseLevel;
            public int numberOfPapers;
            public List<Paper> papers;
        }

        // List of student papers
        public class Paper
        {
            public string name;
            public int mark;
            public string grade;
        }
    }
}
