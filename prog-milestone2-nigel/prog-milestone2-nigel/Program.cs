﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_milestone2_nigel
{
    class Program
    {
        // Main method
        static void Main(string[] args)
        {
            // Initialising main menu to a dictionary
            Dictionary<int, string> mainMenu = new Dictionary<int, string>();
            // Menu heading
            mainMenu.Add(0, "Main Menu");
            // Menu items
            mainMenu.Add(1, "Date Calculator");
            mainMenu.Add(2, "Calculate Grade Average");
            mainMenu.Add(3, "Generate Random Number");
            mainMenu.Add(4, "Rate Favourite Food");
            // Input selection of menu
            int select;

            // Display the main menu and prompt user for input
            do
            {
                // Assign the selection of the main menu
                select = DisplayMenu(mainMenu);

                // Check for selection and run appropriate task
                switch (select)
                {
                    case 1:
                        DateCalculator.Run(FindValue(mainMenu, select));
                        break;
                    case 2:
                        GradeAverage.Run(FindValue(mainMenu, select));
                        break;
                    case 3:
                        RandomNumber.Run();
                        break;
                    case 4:
                        FavouriteFood.Run();
                        break;
                }
            } while (select != mainMenu.Count);
        }

        // Used to pause the screen before the task ends to show what it has done
        public static void PauseScreen()
        {
            Console.WriteLine("\nPress enter to continue...");
            Console.ReadLine();
            return;
        }

        // Method used as a template for different methods to prompt user for input selection
        public static int DisplayMenu(Dictionary<int, string> menu)
        {
            // Clear screen, start fresh with a new menu on the screen
            Console.Clear();

            // Declare variables
            string select;
            int num;

            // Create menu from dictionary passed into this method
            foreach (var m in menu)
            {
                // Do if key is 0, i.e. the heading
                if (m.Key == 0)
                {
                    Heading(m.Value);
                    Console.WriteLine("Please choose an option:");
                }
                // These are the menu items/options
                else
                {
                    Console.WriteLine($"{m.Key}. {m.Value}");
                }
            }
            // Final option to exit the menu
            Console.Write($"{ menu.Count}. Exit\nOption: ");

            // Assign input
            select = Console.ReadLine();

            int.TryParse(select, out num);
            return num;
        }

        // Shared method to display screen heading
        public static void Heading(string heading, string special = "*", bool clear = true)
        {
            if (clear)
            {
                Console.Clear();
            }
            Console.WriteLine(heading);
            for (int i = 0; i < heading.Length; i++)
            {
                Console.Write(special);
            }
            Console.Write("\n");
        }

        // Get value from dictionary
        public static string FindValue(Dictionary<int, string> dictionary, int key)
        {
            string value;
            dictionary.TryGetValue(key, out value);
            return value;
        }

    }
}
