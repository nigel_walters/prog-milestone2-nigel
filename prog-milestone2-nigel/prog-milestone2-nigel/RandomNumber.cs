﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_milestone2_nigel
{
    class RandomNumber
    {
        // Entry point to run this task
        public static void Run()
        {
            // Task not needed to be done as only required to do 2
            Program.Heading("Doing more tasks than needed... Yeah right!");
            Program.PauseScreen();
            return;
        }
    }
}
