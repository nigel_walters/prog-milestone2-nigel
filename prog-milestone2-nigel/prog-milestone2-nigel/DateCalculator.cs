﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_milestone2_nigel
{
    /*
    Date Calculator
      Date Of Birth
        Create a method that does the following
        Allow the user to type in their date of birth
        Tell the user how many days old they are
      Calculate Days
        Create a method that takes a number (which will be the number of years)
        Allow the user to type in that number which will be used in a calculation
        Tell the user how many days the amount of years are that the user typed in
    */
    class DateCalculator
    {
        // Create a variable that holds todays date
        public static DateTime today = DateTime.Now;

        // Run method - main entry point to execute the date calculator task methods
        public static void Run(string s)
        {
            // Instantiate dictionary to hold menu
            Dictionary<int, string> dateCalculator = new Dictionary<int, string>();
            // Menu heading
            dateCalculator.Add(0, s);
            // Menu items
            dateCalculator.Add(1, "How many days old are you?");
            dateCalculator.Add(2, "How many days are in how many years from today?");

            // Variable to hold dictionary value
            string value;
            // Variable to hold menu selection
            int select;

            // Display menu and get input, then execute method accordingly
            do
            {
                // Display menu method assigned to select variable
                select = Program.DisplayMenu(dateCalculator);

                // Check to see if the value exists in the dictionary menu and assign to value
                dateCalculator.TryGetValue(select, out value);

                // Switch case select variable, if select is not one of these then none of these methods will be executed
                switch (select)
                {
                    // Run date of birth method
                    case 1:
                        DateOfBirth(value);
                        break;
                    // Run calculate days method
                    case 2:
                        CalculateDays(value);
                        break;
                }
            }
            // Iterate through the loop while the select variable does not equal the exit option
            while (select != dateCalculator.Count);

            // Go back to the main method/menu of the program
            return;
        }

        // Date of birth method
        private static void DateOfBirth(string s)
        {
            // Declare variables
            // Date of birth
            DateTime dob = new DateTime();
            // Number of days 
            double days;
            // Rounded number of days
            double _days;
            // Check variable to set if input is a date
            bool isDate;

            // Call heading method to display the task being executed
            Program.Heading(s);

            // Prompt user for a date and show an error message if the input is not a date
            do
            {
                // Display message to prompt for date of birth
                Console.Write("Please enter your date of birth: ");
                // Check if input is a valid date
                isDate = DateTime.TryParse(Console.ReadLine(), out dob);
                // If the date is not valid show error message
                if (!isDate)
                {
                    Console.WriteLine("Error! Please enter date in correct format DD/MM/YYYY\n");
                }
            }
            // Loop while the input is not a date
            while (!isDate);

            // Subtract date of birth from todays date and store the difference in days variable
            days = (today - dob).TotalDays;

            // Round the days variable down to the next whole day, given that you would not round up a partial day
            _days = Math.Floor(days);

            // Display the message of how many days old you from today based on your date of birth
            Console.WriteLine($"\nYou are {_days} days old!");

            // Pause screen method to help display results before returning the method
            Program.PauseScreen();
            return;
        }

        // Calculate days method
        private static void CalculateDays(string s)
        {
            // Declare/Intialise variables
            double days;
            DateTime date = new DateTime();
            int years;
            int _years = 9999 - today.Year;
            bool isNum;

            // Display task heading
            Program.Heading(s);

            // Prompt user for input and check for validity
            do
            {
                Console.Write("Please enter the number of years: ");
                isNum = int.TryParse(Console.ReadLine(), out years);

                // If number is not valid display error message
                if (!isNum || years <= 0 || years > _years)
                {
                    Console.WriteLine($"Error! Please enter a number between 0 and {_years}\n");
                    isNum = false;
                }
            } while (!isNum);

            // Add the amount of years to todays date
            date = today.AddYears(years);
           
            // Subtract the date from today and get total days
            days = (date - today).TotalDays;

            // Display message
            Console.WriteLine($"\n{years} years from now will be {date.Date.ToShortDateString()}, and is {days} days from today");

            Program.PauseScreen();
            return;
        }
    }
}
